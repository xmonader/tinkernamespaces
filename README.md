having multiple projects following the same namespace (https://www.python.org/dev/peps/pep-0420/) structure


```
proj1/
    /jumpscale
        /clients
        /sals
        ...

proj2/
    /jumpscale
        /clients
        /sals
        ...
```

- jumpscale needs to be namespace (no `__init__.py`)
- clients, sals, .. need to be namespaces (no `__init__.py`)
- loader in `projgod` (used using `from jumpscale.god import j`)


